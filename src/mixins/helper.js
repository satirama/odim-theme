import sal from 'sal.js';

export const helper = {
  methods: {
    animate(options) {
      document.querySelectorAll(options.selector).forEach(el => {
        el.dataset.sal = options.sal;
        el.dataset.salDuration = options.duration;
        el.dataset.salEasing = options.easing;
        el.dataset.salDelay = options.delay;
        el.addEventListener('sal:in', () => {
          //console.log('entering', detail.target);
        });
      });
      sal({
        threshold: options.threshold,
        once: options.once,
        selector: options.selector
      });
    }
  }
}